﻿using Collections_LINQ.Interfaces;
using Common.DTOs.FunctionalModels;
using Common.DTOs.TaskModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TaskService : ITaskService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TaskService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<TaskModelDTO> GetTaskById(int id)
        {
            var task = await client.GetStringAsync($"{baseUrl}/api/tasks/{id}");
            return JsonConvert.DeserializeObject<TaskModelDTO>(task);
        }

        public async Task<ICollection<TaskModelDTO>> GetTasks()
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks");
            return JsonConvert.DeserializeObject<ICollection<TaskModelDTO>>(tasks);
        }

        public async Task<TaskModelDTO> CreateTask(TaskModelCreateDTO task)
        {
            var response = await client.PostAsJsonAsync($"{baseUrl}/api/tasks", task);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModelDTO>();
        }

        public async Task<TaskModelDTO> UpdateTask(TaskModelUpdateDTO task)
        {
            var response = await client.PutAsJsonAsync($"{baseUrl}/api/tasks", task);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModelDTO>();
        }

        public async Task DeleteTask(int id)
        {
            var response = await client.DeleteAsync($"{baseUrl}/api/tasks/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<TaskModelDTO>> GetTasksByUserIdWithNameCondition(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/by-user-with-name-condition/{id}");
            return JsonConvert.DeserializeObject<IEnumerable < TaskModelDTO >> (tasks);
        }

        public async Task<IEnumerable<TaskFinishedThisYearDTO>> GetTasksByUserFinishedThisYear(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/by-user-finished-this-year/{id}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskFinishedThisYearDTO>>(tasks);
        }

        public async Task<IEnumerable<User_TasksDTO>> GetUsersSortedByFirstNameAndSortedTasks()
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/users-sorted-by-first-name-sorted-tasks");
            return JsonConvert.DeserializeObject<IEnumerable<User_TasksDTO>>(tasks);
        }

        public async Task<IEnumerable<UserLastProjectTasksDTO>> GetNewUserStructure(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/structure-with-tasks-by-user/{id}");
            return JsonConvert.DeserializeObject<IEnumerable<UserLastProjectTasksDTO>>(tasks);
        }
    }
}
