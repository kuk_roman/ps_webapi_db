﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class TaskStateModel : BaseEntity
    {
        public string Value { get; set; }
    }
}
