﻿using DAL.Entities.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: BaseEntity
    {
        void Create(TEntity entity, string createdBy = null);
        void Update(TEntity entity, string updatedBy = null);
        void DeleteById(int id);
        void Delete(TEntity entity);
        DbSet<TEntity> GetAll();
        Task<TEntity> GetById(int id);
    }
}
