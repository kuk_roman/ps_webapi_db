﻿using DAL.Context;
using DAL.Entities.Abstract;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ModelsContext context;

        public Repository(ModelsContext context)
        {
            this.context = context;
        }
        
        public void Create(TEntity entity, string createdBy = null)
        {
            //if (entity.Id == 0)
            //    entity.Id = context.Set<TEntity>().LastOrDefault().Id + 1;
            context.Set<TEntity>().Add(entity);
        }

        public void DeleteById(int id)
        {
            TEntity entity = context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            var dbSet = context.Set<TEntity>();
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public void Update(TEntity entity, string updatedBy = null)
        {
            context.Set<TEntity>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public DbSet<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public Task<TEntity> GetById(int id)
        {
            return Task.Run(() => context.Set<TEntity>().Find(id));
        }
    }
}
