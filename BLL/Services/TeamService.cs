﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.Team;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : BaseService
    {
        private readonly IRepository<Team> repository;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<Team>();
        }

        public async Task<ICollection<TeamDTO>> GetAllTeams()
        {
            var teams = await Task.Run(() => repository.GetAll());

            return mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var teamEntity = await repository.GetById(id);
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            return (await GetAllTeams()).FirstOrDefault(t => t.Id == id);
        }

        public async Task<TeamDTO> CreateTeam(TeamCreateDTO teamDto)
        {
            var teamEntity = mapper.Map<Team>(teamDto);
            teamEntity.CreatedAt = DateTime.Now;

            repository.Create(teamEntity);
            await unitOfWork.SaveChangesAsync();

            return (await GetAllTeams()).FirstOrDefault(p => p.Id == teamEntity.Id);
        }

        public async Task<TeamDTO> UpdateTeam(TeamUpdateDTO teamDto)
        {
            var teamEntity = await repository.GetById(teamDto.Id);
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), teamDto.Id);
            }

            teamEntity.Name = teamDto.Name;

            repository.Update(teamEntity);
            await unitOfWork.SaveChangesAsync();

            return (await GetAllTeams()).FirstOrDefault(p => p.Id == teamEntity.Id);
        }

        public async Task DeleteTeamById(int id)
        {
            var teamEntity = repository.GetById(id);
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

    }
}
