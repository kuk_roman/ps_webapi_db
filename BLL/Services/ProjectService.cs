﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.Project;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService : BaseService
    {
        private readonly IRepository<Project> repository;
        private readonly TeamService teamService;
        private readonly UserService userService;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper,
            TeamService teamService, UserService userService) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<Project>();
            this.teamService = teamService;
            this.userService = userService;
        }

        public async Task<ICollection<ProjectDTO>> GetAllProjects()
        {
            var projects = await Task.Run(() => 
                repository.GetAll()
                    .Include(p => p.Author)
                        .ThenInclude(a => a.Team)
                    .Include(p => p.Team)
                    .Include(p => p.Tasks)
                        .ThenInclude(t => t.Performer)
                        .ThenInclude(p => p.Team));

            return mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var projectCheck = await repository.GetById(id);
            if (projectCheck == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            return (await GetAllProjects()).FirstOrDefault(p => p.Id == id);
        }

        public async Task<ProjectDTO> CreateProject(ProjectCreateDTO projectDto)
        {
            var projectEntity = mapper.Map<Project>(projectDto);
            projectEntity.CreatedAt = DateTime.Now;

            await teamService.GetTeamById(projectEntity.TeamId);
            await userService.GetUserById(projectEntity.AuthorId);

            repository.Create(projectEntity);
            await unitOfWork.SaveChangesAsync();

            return (await GetAllProjects()).FirstOrDefault(p => p.Id == projectEntity.Id);
        }

        public async Task<ProjectDTO> UpdateProject(ProjectUpdateDTO projectDto)
        {
            var projectEntity = await repository.GetById(projectDto.Id);

            if (projectEntity == null)
            {
                throw new NotFoundException(nameof(Project), projectDto.Id);
            }

            await teamService.GetTeamById(projectDto.TeamId);
            await userService.GetUserById(projectDto.AuthorId);

            projectEntity.Name = projectDto.Name;
            projectEntity.Description = projectDto.Description;
            projectEntity.Deadline = projectDto.Deadline;
            projectEntity.AuthorId = projectDto.AuthorId;
            projectEntity.TeamId = projectDto.TeamId;

            repository.Update(projectEntity);
            await unitOfWork.SaveChangesAsync();

            return (await GetAllProjects()).FirstOrDefault(p => p.Id == projectEntity.Id);
        }

        public async Task DeleteProjectById(int id)
        {
            var projectEntity = await repository.GetById(id);
            if (projectEntity == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async Task<Dictionary<ProjectDTO, int>> GetUserProjectTasksCountByUserId(int userId)
        {
            var projects = (await GetAllProjects())
                .Where(pr => pr.Author.Id == userId)
                .GroupBy(pr => pr)
                .ToDictionary(item => item.Key, item => item.Key.Tasks.Count());

            return projects;
        }

        public async Task<IEnumerable<Project_TasksDTO>> CreateNewProjectStructureAsync()
        {
            var teams = await teamService.GetAllTeams();
            var users = (await userService.GetAllUsers()).Where(u => u.Team != null);
            var structure = (await GetAllProjects())
                .Join(teams,
                    project => project.Team.Id,
                    team => team.Id,
                    (pr, t) => new Project_TasksDTO
                    {
                        Project = pr,
                        LongestTaskByDescription = pr.Tasks
                            .OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                        ShortestTaskByName = pr.Tasks
                            .OrderBy(task => task.Name.Length).FirstOrDefault(),
                        TotalUsersCount = teams
                        .Where(team => team.Id == t.Id)
                        .GroupJoin(users,
                            team => team.Id,
                            user => user.Team.Id,
                            (t, u) =>
                            {
                                if (pr.Description.Length > 20 || pr.Tasks.Count() < 3)
                                    return u.Count();
                                else
                                    return 0;
                            })
                        .FirstOrDefault()
                    });
            return structure;
        }
    }
}
