﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs.FunctionalModels;
using Common.DTOs.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService userService;

        public UsersController(UserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> Get()
        {
            return Ok(await userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            return Ok(await userService.GetUserById(id));
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Create([FromBody] UserCreateDTO dto)
        {
            return Ok(await userService.CreateUser(dto));
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserUpdateDTO team)
        {
            return Ok(await userService.UpdateUser(team));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(int id)
        {
            await userService.DeleteUserById(id);
            return NoContent();
        }

        [HttpGet("team-users-sorted-by-registered-date")]
        public async Task<ActionResult<IEnumerable<Team_UsersDTO>>> GetTeamsUsersSortedByRegisteredDate()
        {
            return Ok(await userService.GetTeamsUsersSortedByRegisteredDateAsync());
        }
    }
}
